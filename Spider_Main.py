import urllib, URL_Manager, HTML_Downloader, HTML_Parser, HTML_Outputer

class SpiderMain(object):

    def __init__(self):
        self.URLs = URL_Manager.URLManager()
        self.Downloader = HTML_Downloader.HTMLDownloader()
        self.Parser = HTML_Parser.HTMLParser()
        self.Outputer = HTML_Outputer.HTMLOutputer()

    def craw(self, root_URL):
        count = 1
        self.URLs.addNewURL(root_URL)
        while self.URLs.hasNewURL():

            newURL = self.URLs.getNewURL()
            print("craw %d : %s" % (count, newURL))
            HTML_Cont = self.Downloader.download(newURL)
            newURLs, newData = self.Parser.parse(newURL, HTML_Cont)
            self.URLs.addNewURLs(newURLs)
            self.Outputer.collectData(newData)

            if count == 10:
                break

            count = count + 1

        self.Outputer.outputHTML()

if __name__ == "__main__":
    root_URL = "http://www.aal.hku.hk/summerinstitute/programme/math1641/"
    obj_spider = SpiderMain()
    obj_spider.craw(root_URL)
