class HTMLOutputer(object):
    def __init__(self):
        self.datas = []

    def collectData(self, data):
        if data is None:
            return
        self.datas.append(data)

    def outputHTML(self):
        fout = open("output.html", "w")

        fout.write("<html>\n")
        fout.write("<body>\n")
        fout.write("<table>\n")

        for data in self.datas:
            fout.write("<tr>\n")
            fout.write("<td>%s</td>\n" % data["URL"])
            fout.write("<td>%s</td>\n" % data["courseName"].lstrip("b'").rstrip("'"))
            fout.write("<td>%s</td>\n" % data["courseCode"].lstrip("b'").rstrip("'"))
            fout.write("</tr>\n")

        fout.write("</table>\n")
        fout.write("</body>\n")
        fout.write("</html>\n")

        fout.close()