import urllib.request

class HTMLDownloader(object):
    def download(self, URL):
        if URL is None:
            return None

        response = urllib.request.urlopen(URL)

        if response.getcode() != 200:
            return None

        return response.read()
