class URLManager(object):
    def __init__(self):
        self.newURLs = set()
        self.oldURLs = set()

    def addNewURL(self, URL):
        if URL is None:
            return
        if URL not in self.newURLs and URL not in self.oldURLs:
            self.newURLs.add(URL)

    def addNewURLs(self, URLs):
        if URLs is None or len(URLs) == 0:
            return
        for URL in URLs:
            self.addNewURL(URL)

    def hasNewURL(self):
        return len(self.newURLs) != 0

    def getNewURL(self):
        newURL = self.newURLs.pop()
        self.oldURLs.add(newURL)
        return newURL