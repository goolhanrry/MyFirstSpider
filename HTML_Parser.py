from bs4 import BeautifulSoup
import re

class HTMLParser(object):
    def _getNewURLs(self, soup):
        newURLs = set()
        links = soup.find_all("a", href=re.compile(r"^http://www.aal.hku.hk/summerinstitute/programme/"))
        for link in links:
            newURL = link["href"]
            newURLs.add(newURL)
        return newURLs

    def _getNewData(self, pageURL, soup):
        resData = {}

        resData["URL"] = pageURL

        nameNode = soup.find("h1", class_="entry-title")
        resData["courseName"] = nameNode.get_text()

        codeNode = soup.find("p", class_="course-code")
        resData["courseCode"] = codeNode.get_text()

        return resData

    def parse(self, pageURL, HTMLCont):
        if pageURL is None or HTMLCont is None:
            return

        soup = BeautifulSoup(HTMLCont, "html.parser", from_encoding="utf-8")
        newURLs = self._getNewURLs(soup)
        newData = self._getNewData(pageURL, soup)
        return newURLs, newData